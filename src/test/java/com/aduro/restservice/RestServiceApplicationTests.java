package com.aduro.restservice;

import com.aduro.restservice.entity.Employee;
import com.aduro.restservice.service.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Optional;

@SpringBootTest
class RestServiceApplicationTests {

	@Autowired
	private EmployeeService employeeService;

	@Test
	void saveEmployee_IsCreated() {
		Employee employee = createEmployee();
		// saving employee into h2
		employeeService.saveEmployee(employee);
		// getting the employee
		Employee employeeFound = employeeService.findTopByOrderByIdDesc();
		// assertions
		Assertions.assertEquals(employee, employeeFound);
	}

	@Test
	void saveEmployeeAndDeleteIt_IsDeleted(){
		Employee employee = createEmployee();
		// saving Employee into h2
		employeeService.saveEmployee(employee);
		// getting the Employee
		Employee employeeFound = employeeService.findTopByOrderByIdDesc();
		// assertions
		Assertions.assertEquals(employee, employeeFound);
		employeeService.deleteEmployee(employeeFound.getId());
		employeeFound = employeeService.findTopByOrderByIdDesc();
		Assertions.assertNull(employeeFound);
	}

	@Test
	void updateEmployee_IsUpdated(){
		Employee employee = createEmployee();
		// saving employee into h2
		employeeService.saveEmployee(employee);
		// getting the employee
		Employee employeeFound = employeeService.findTopByOrderByIdDesc();
		// assertions
		Assertions.assertEquals(employee, employeeFound);
		// updating employee office
		Employee newEmployee = employeeFound;
		newEmployee.setOffice("421b");
		employeeService.updateEmployee(newEmployee);
		employeeFound = employeeService.findTopByOrderByIdDesc();
		employee.setOffice("421b");
		// assertions
		Assertions.assertEquals(employeeFound, employee);
	}

	@Test
	void verifyMultipleEmployees_AreCreated(){
		Employee employee = createEmployee1();
		// saving employee into h2
		employeeService.saveEmployee(employee);
		// getting the employee
		Employee employeeFound = employeeService.findTopByOrderByIdDesc();
		// assertions
		Assertions.assertEquals(createEmployee1().getName(), employeeFound.getName());

		employee = createEmployee2();
		// saving employee into h2
		employeeService.saveEmployee(employee);
		// getting the employee
		employeeFound = employeeService.findTopByOrderByIdDesc();
		// assertions
		Assertions.assertEquals(createEmployee2().getName(), employeeFound.getName());

		employee = createEmployee3();
		// saving employee into h2
		employeeService.saveEmployee(employee);
		// getting the employee
		employeeFound = employeeService.findTopByOrderByIdDesc();
		// assertions
		Assertions.assertEquals(createEmployee3().getName(), employeeFound.getName());

		employee = createEmployee4();
		// saving employee into h2
		employeeService.saveEmployee(employee);
		// getting the employee
		employeeFound = employeeService.findTopByOrderByIdDesc();
		// assertions
		Assertions.assertEquals(createEmployee4().getName(), employeeFound.getName());

		employee = createEmployee5();
		// saving employee into h2
		employeeService.saveEmployee(employee);
		// getting the employee
		employeeFound = employeeService.findTopByOrderByIdDesc();
		// assertions
		Assertions.assertEquals(createEmployee5().getName(), employeeFound.getName());

		employee = createEmployee6();
		// saving employee into h2
		employeeService.saveEmployee(employee);
		// getting the employee
		employeeFound = employeeService.findTopByOrderByIdDesc();
		// assertions
		Assertions.assertEquals(createEmployee6().getName(), employeeFound.getName());
	}

	@Test
	void verifyValidations_ExceptionThrown(){

		// Name with numbers
		Assertions.assertThrows(TransactionSystemException.class, () -> {
			Employee employee = new Employee();
			employee.setName("Michael Stone with numbers 555");
			employee.setOffice("321b");
			employee.setEmail("michael.stone@oscorp.com");
			employee.setPhone("415.331.3321");
			employee.setRole("Teir 3 Support Engineer");
			employeeService.saveEmployee(employee);
		});

		// Office with different format
		Assertions.assertThrows(TransactionSystemException.class, () -> {
			Employee employee = new Employee();
			employee.setName("Michael Stone");
			employee.setOffice("k21b");
			employee.setEmail("michael.stone@oscorp.com");
			employee.setPhone("415.331.3321");
			employee.setRole("Teir 3 Support Engineer");
			employeeService.saveEmployee(employee);
		});

		// Not valid email
		Assertions.assertThrows(TransactionSystemException.class, () -> {
			Employee employee = new Employee();
			employee.setName("Michael Stone");
			employee.setOffice("321b");
			employee.setEmail("michael.stoneoscorp.com");
			employee.setPhone("415.331.3321");
			employee.setRole("Teir 3 Support Engineer");
			employeeService.saveEmployee(employee);
		});

		// Phone with different format
		Assertions.assertThrows(TransactionSystemException.class, () -> {
			Employee employee = new Employee();
			employee.setName("Michael Stone");
			employee.setOffice("321b");
			employee.setEmail("michael.stone@oscorp.com");
			employee.setPhone("415533153321");
			employee.setRole("Teir 3 Support Engineer");
			employeeService.saveEmployee(employee);
		});

		// Role with more than 150 chars
		Assertions.assertThrows(TransactionSystemException.class, () -> {
			Employee employee = new Employee();
			employee.setName("Michael Stone");
			employee.setOffice("321b");
			employee.setEmail("michael.stone@oscorp.com");
			employee.setPhone("415.331.3321");
			employee.setRole("Teir 3 Support Engineer Teir 3 Support Engineer Teir 3 Support Engineer Teir 3 Support Engineer Teir 3 Support Engineer Teir 3 Support Engineer Teir 3 Support Engineer");
			employeeService.saveEmployee(employee);
		});

	}

	private Employee createEmployee(){
		Employee employee = new Employee();
		employee.setName("Michael Stone");
		employee.setOffice("321b");
		employee.setEmail("michael.stone@oscorp.com");
		employee.setPhone("415.331.3321");
		employee.setRole("Teir 3 Support Engineer");
		return employee;
	}

	private Employee createEmployee1(){
		Employee employee = new Employee();
		employee.setId(5);
		employee.setName("Michael Stone");
		employee.setOffice("321b");
		employee.setEmail("michael.stone@oscorp.com");
		employee.setPhone("415.331.3321");
		employee.setRole("Teir 3 Support Engineer");
		return employee;
	}

	private Employee createEmployee2(){
		Employee employee = new Employee();
		employee.setId(8);
		employee.setName("Amber Johnson");
		employee.setOffice("332c");
		employee.setEmail("amber.johnson@oscorp.com");
		employee.setPhone("415.337.4345");
		employee.setRole("Senior Project Manager");
		return employee;
	}

	private Employee createEmployee3(){
		Employee employee = new Employee();
		employee.setId(11);
		employee.setName("Jung Wei-Li");
		employee.setOffice("305a");
		employee.setEmail("jung.wei-li@oscorp.com");
		employee.setPhone("415.221.8810");
		employee.setRole("SEO Analyst");
		return employee;
	}

	private Employee createEmployee4(){
		Employee employee = new Employee();
		employee.setId(15);
		employee.setName("Cliff Rosen");
		employee.setOffice("361d");
		employee.setEmail("clifford.rosen@oscorp.com");
		employee.setPhone("415.654.6010");
		employee.setRole("Operations Analyst");
		return employee;
	}

	private Employee createEmployee5(){
		Employee employee = new Employee();
		employee.setId(16);
		employee.setName("Maria Velasquez");
		employee.setOffice("345d");
		employee.setEmail("maria.velasquez@oscorp.com");
		employee.setPhone("415.541.9819");
		employee.setRole("Senior Data Scientist");
		return employee;
	}

	private Employee createEmployee6(){
		Employee employee = new Employee();
		employee.setId(22);
		employee.setName("Carol Dawson");
		employee.setOffice("312A");
		employee.setEmail("carol.dawson@oscorp.com");
		employee.setPhone("415.212.0045");
		employee.setRole("Software Development Engineer II");
		return employee;
	}

}
