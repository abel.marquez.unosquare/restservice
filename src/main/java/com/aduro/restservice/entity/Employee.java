package com.aduro.restservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @NotBlank
    @Size(max = 100)
    @Pattern(regexp = "^[a-zA-Z\\s-]+$",
             message = "The name: '${validatedValue}' must contain only characters")
    private String name;
    @NotBlank
    @Pattern(regexp = "^[1-5]{1}[0-9]{2}[a-fA-F]{1}$",
             message = "The office: '${validatedValue}' must be 4 characters long and follow the expression ^[1-5]{1}[0-9]{2}[a-fA-F]{1}$")
    private String office;
    @NotBlank
    @Pattern(regexp = "^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])",
             message = "The email: '${validatedValue}' is not well-formed")
    private String email;
    @NotBlank
    @Pattern(regexp = "^[0-9]{3}\\.[0-9]{3}\\.[0-9]{4}$",
             message = "The phone: '${validatedValue}' should follow the expression '^[0-9]{3}\\.[0-9]{3}\\.[0-9]{4}$'")
    private String phone;
    @NotBlank
    @Size(max = 150)
    @Pattern(regexp = "^[a-zA-Z0-9\\s]+$",
            message = "The role: '${validatedValue}' must contain only characters")
    private String role;

}
