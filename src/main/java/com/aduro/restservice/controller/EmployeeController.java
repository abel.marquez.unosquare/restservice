package com.aduro.restservice.controller;

import com.aduro.restservice.entity.Employee;
import com.aduro.restservice.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/employees")
@Validated
@Slf4j
public class EmployeeController {

    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping("/save")
    public ResponseEntity<Employee> saveEmployee(@Valid @RequestBody Employee employee) {
        employeeService.saveEmployee(employee);
        log.info("Employee: " + employee.getName() + " saved");
        return ResponseEntity.ok(employee);
    }

    @PostMapping("/saveAll")
    public ResponseEntity<List<Employee>> saveEmployees(@Valid @RequestBody List<Employee> employees) {
        employeeService.saveEmployees(employees);
        log.info(employees.size() + " Employees saved");
        return ResponseEntity.ok(employees);
    }

    @GetMapping("/")
    public ResponseEntity<List<Employee>> getAllEmployees() {
        return ResponseEntity.ok(employeeService.getAllEmployees());
    }

    @GetMapping("/find")
    public ResponseEntity<Employee> getEmployee(@RequestParam int id) {
        Optional<Employee> employee = employeeService.findById(id);
        if(employee.isPresent()){
            return ResponseEntity.ok(employee.get());
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/update")
    public ResponseEntity<String> updateEmployee(@Valid @RequestBody Employee employee, @RequestParam int id) {
        // Setting id from param
        employee.setId(id);
        Optional<Employee> savedEmployee = employeeService.updateEmployee(employee);
        if(savedEmployee.isPresent()){
            log.info("Employee: " + employee.getName() + " updated");
            return ResponseEntity.ok(employee.getName() + " updated");
        } else {
            log.info("Employee with id: " + employee.getId() + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete")
    public ResponseEntity<String> deleteEmployee(@RequestParam int id) {
        Optional<Employee> deletedEmployee = employeeService.deleteEmployee(id);
        if(deletedEmployee.isPresent()){
            log.info("Employee with id: " + id + " deleted");
            return ResponseEntity.ok("Employee with id: " + id + " deleted");
        } else {
            log.info("Employee with id: " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseEntity<String> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        log.error(e.getMessage());
        return new ResponseEntity<>("The following exception occurred during validations: " + e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseEntity<String> handleConstraintViolationException(ConstraintViolationException e) {
        log.error(e.getMessage());
        return new ResponseEntity<>("The following exception occurred during validations: " + e.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
