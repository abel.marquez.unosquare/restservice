package com.aduro.restservice.service;

import com.aduro.restservice.entity.Employee;
import com.aduro.restservice.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Transactional
    public Employee saveEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Transactional
    public Optional<Employee> updateEmployee(Employee employee) {
        Optional<Employee> employeeFound = findById(employee.getId());
        if(employeeFound.isPresent()){
            employeeRepository.save(employee);
        }
        return employeeFound;
    }

    @Transactional
    public List<Employee> saveEmployees(List<Employee> employees) {
        return employeeRepository.saveAll(employees);
    }

    public List<Employee> getAllEmployees(){
        return employeeRepository.findAll();
    }
    
    @Transactional
    public Optional<Employee> deleteEmployee(int id) {
        Optional<Employee> employeeFound = findById(id);
        if (employeeFound.isPresent()) {
            employeeRepository.deleteById(id);
        }
        return employeeFound;
    }

    public Optional<Employee> findById(int id){
        return employeeRepository.findById(id);
    }

    public Employee findTopByOrderByIdDesc(){
        return employeeRepository.findTopByOrderByIdDesc();
    }
}
